package com.sd.laborator.controllers

import com.sd.laborator.interfaces.BudgetService
import com.sd.laborator.pojo.Payment
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class BudgetController {
    @Autowired
    private lateinit var budgetService: BudgetService

    @RequestMapping(value = ["/payment"], method = [RequestMethod.POST])
    fun createPayment(@RequestBody payment: Payment): ResponseEntity<Unit> {
        budgetService.createPayment(payment)
        return ResponseEntity(Unit, HttpStatus.CREATED)
    }

    @RequestMapping(value = ["/payment/{id}"], method = [RequestMethod.GET])
    fun getPayment(@PathVariable id: Int): ResponseEntity<Payment?> {
        val payment: Payment? = budgetService.getPayment(id)
        val status = if (payment == null) {
            HttpStatus.NOT_FOUND
        } else {
            HttpStatus.OK
        }
        return ResponseEntity(payment, status)
    }

    @RequestMapping(value = ["/payment/{id}"], method = [RequestMethod.PUT])
    fun updatePayment(@PathVariable id: Int, @RequestBody payment: Payment): ResponseEntity<Unit> {
        budgetService.getPayment(id)?.let {
            budgetService.updatePayment(it.id, payment)
            return ResponseEntity(Unit, HttpStatus.ACCEPTED)
        } ?: return ResponseEntity(Unit, HttpStatus.NOT_FOUND)
    }

    @RequestMapping(value = ["/payment/{id}"], method = [RequestMethod.DELETE])
    fun deletePayment(@PathVariable id: Int): ResponseEntity<Unit> {
        if (budgetService.getPayment(id) != null) {
            budgetService.deletePayment(id)
            return ResponseEntity(Unit, HttpStatus.OK)
        } else {
            return ResponseEntity(Unit, HttpStatus.NOT_FOUND)
        }
    }

    @RequestMapping(value = ["/payments"], method = [RequestMethod.GET])
    fun search():
            ResponseEntity<List<Payment>> {
        val personList = budgetService.search()
        var httpStatus = HttpStatus.OK
        if (personList.isEmpty()) {
            httpStatus = HttpStatus.NO_CONTENT
        }
        return ResponseEntity(personList, httpStatus)
    }
}