package com.sd.laborator.services;

import com.sd.laborator.interfaces.BudgetService
import com.sd.laborator.pojo.Payment
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap

@Service
class BudgetServiceImpl : BudgetService {
    companion object {
        val initialAgenda = arrayOf(
            Payment(1,  "65.7", "Gas Bill", "Bill"),
            Payment(2, "43.1", "TV Bill", "Bill"),
            Payment(3, "100.1", "Food groceries", "Food"),
            Payment(4, "20.0", "Child meditation", "Education")
        )
    }

    private val agenda = ConcurrentHashMap<Int, Payment>(
        initialAgenda.associateBy { payment: Payment -> payment.id }
    )

    override fun getPayment(id: Int): Payment? {
        return agenda[id]
    }

    override fun createPayment(payment: Payment) {
        agenda[payment.id] = payment
    }

    override fun deletePayment(id: Int) {
        agenda.remove(id)
    }

    override fun updatePayment(id: Int, payment: Payment) {
//        deletePayment(id)
//        createPayment(payment)
        agenda[id]=payment
    }

    override fun search(): List<Payment> {
        return agenda.map {
            it.value
        }.toList()
    }
}