package com.sd.laborator.interfaces

import com.sd.laborator.pojo.Payment

interface BudgetService {
    fun getPayment(id: Int) : Payment?
    fun createPayment(payment: Payment)
    fun deletePayment(id: Int)
    fun updatePayment(id: Int, payment: Payment)
    fun search(): List<Payment>
}