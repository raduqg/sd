package com.sd.laborator.pojo

data class Payment(
    var id: Int = 0,
    var Cost: String,
    var Name:String,
    var Type:String
)