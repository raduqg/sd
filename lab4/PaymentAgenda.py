from flask import Flask, request, json
import requests

app = Flask(__name__)


@app.route('/index')
def welcome():
    return '''
    <h1>Welcome to your family payments agenda</h1><br/>
    <a href= http://127.0.0.1:5000/payments>Search payments</a><br/>
    <a href=http://127.0.0.1:5000/payment>Insert new payment</a><br/>
    <a href=http://127.0.0.1:5000/get>Get payment by id</a><br/>
    <a href=http://127.0.0.1:5000/delete>Delete payment</a><br/>
    <a href=http://127.0.0.1:5000/update>Update payment</a><br/>
    '''


@app.route('/payment', methods=['GET', 'POST'])
def payment():
    if request.method == 'POST':
        headers = {'content-type': 'application/json'}
        url= "http://localhost:8080/payment"
        r = requests.post(url, data=json.dumps(request.form), headers=headers)

        return r.content
    return '''
    <p>Complete info about payment</p>
        <form method="post">
            <p><label for=id>Id:</label>
             <input type=text name=id>
             <p><label for=Cost>Cost:</label>
            <input type=text name=Cost>
            <p><label for=Name>Name:</label>
            <input type=text name=Name>
            <p><label for=Type>Type:</label>
            <input type=text name=Type>
            <p><input type=submit value=Create>
        </form>
    '''


@app.route('/payments', methods=['GET'])
def getpayments():

    url = "http://localhost:8080/payments"
    r = requests.get(url)
    formated_response = "Payments:<br>"
    result = json.loads(r.content)
    formated_response += "<br>".join( f"Id:{it['id']}, Name:{it['name']}, Cost:{it['cost']}, Type:{it['type']}" for it in result)
    return formated_response


@app.route('/delete', methods=['GET', 'POST'])
def deletepayment():
    if request.method == 'POST':
        id=request.form.get("id")
        url= f"http://localhost:8080/payment/{id}"
        r = requests.delete(url)
        if r.status_code == 404:
            return "ERROR! Payment with this id does not exist"
        else:
            return "Payment deleted"
    return '''
    <p>Delete payment</p>
        <form method="post">
            <p><label for=id>Id:</label>
             <input type=text name=id>
            <p><input type=submit value=DELETE>
        </form>
    '''


@app.route('/get', methods=['GET', 'POST'])
def getpayment():
    if request.method == 'POST':
        id=request.form.get("id")
        url= f"http://localhost:8080/payment/{id}"
        r = requests.get(url)
        if r.status_code == 404:
            return "ERROR! Payment with this id does not exist"
        else:
            return r.content
    return '''
    <p>Get payment by id</p>
        <form method="post">
            <p><label for=id>Id:</label>
             <input type=text name=id>
            <p><input type=submit value=GET>
        </form>
    '''

@app.route('/update', methods=['GET', 'POST'])
def updatepayment():
    if request.method == 'POST':
        headers = {'content-type': 'application/json'}
        id=request.form.get("id")
        url= f"http://localhost:8080/payment/{id}"
        r = requests.put(url, data=json.dumps(request.form), headers=headers)
        if r.status_code == 404:
            return "ERROR! Payment with this id does not exist"
        else:
            return "Payment updated"
    return '''
    <p>Update payment</p>
        <form method="post">
            <p><label for=id>Id:</label>
             <input type=text name=id>
             <p><label for=Cost>Cost:</label>
            <input type=text name=Cost>
            <p><label for=Name>Name:</label>
            <input type=text name=Name>
            <p><label for=Type>Type:</label>
            <input type=text name=Type>
            <p><input type=submit value=UPDATE>
        </form>
    '''


if __name__ == '__main__':
    app.run()
