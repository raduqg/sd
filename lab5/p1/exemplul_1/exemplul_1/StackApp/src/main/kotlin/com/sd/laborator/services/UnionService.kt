package com.sd.laborator.services

import com.sd.laborator.interfaces.CartesianProductOperation
import com.sd.laborator.interfaces.PrimeNumberGenerator
import com.sd.laborator.interfaces.UnionOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UnionService: UnionOperation {

    @Autowired
    private lateinit var cartesianProductOperation: CartesianProductOperation

    override fun executeOperation(A: Set<Int>, B: Set<Int>): Set<Pair<Int, Int>> {
        val partialResult1 = cartesianProductOperation.executeOperation(A, B)
        val partialResult2 = cartesianProductOperation.executeOperation(B, B)
        return partialResult1 union partialResult2
    }
    override fun generatePrimeNumber(): Int
    {
        return cartesianProductOperation.generatePrimeNumber()
    }

}