import os
import sys
from PyQt5.QtWidgets import QWidget,QLineEdit,QTextEdit, QLabel, QApplication, QFileDialog, QMessageBox, QPushButton
from PyQt5 import QtCore
from PyQt5.uic import loadUi
from mq_communication import ApacheMQ


def debug_trace(ui=None):
    from pdb import set_trace
    QtCore.pyqtRemoveInputHook()
    set_trace()
    # QtCore.pyqtRestoreInputHook()

class AddBookWidget(QWidget ):
    def __init__(self,LibraryApp):
        super().__init__()
        self.back=LibraryApp
        self.setWindowTitle("Adaugare carte")
        self.name_lbl= QLabel("Name:", self)
        self.name_lbl.move(20,20)
        self.name_txt=QLineEdit(self)
        self.name_txt.move(70,20)
        self.autor_lbl= QLabel("Autor:", self)
        self.autor_lbl.move(20,50)
        self.autor_txt=QLineEdit(self)
        self.autor_txt.move(70,50)
        self.edit_lbl= QLabel("Editura:", self)
        self.edit_lbl.move(20,80)
        self.edit_txt=QLineEdit(self)
        self.edit_txt.move(70,80)
        self.cont_lbl=QLabel("Continut:", self)
        self.cont_lbl.move(20,110)
        self.cont_txt=QTextEdit(self)
        self.cont_txt.resize(200,200)
        self.cont_txt.move(70,110)
        self.nf_Add_btn= QPushButton("Add", self)
        self.nf_Add_btn.clicked.connect(self.add_book)
        self.nf_Add_btn.move(275,200)
        self.show()

    def add_book(self):
        body="Name:"+self.name_txt.text()+",Autor:"+self.autor_txt.text()+",Editura:"+self.edit_txt.text()+",Continut:"+self.cont_txt.toPlainText()
        self.back.add_book(body)
        self.hide()
        

class LibraryApp(QWidget):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

    def __init__(self):
        super(LibraryApp, self).__init__()
        ui_path = os.path.join(self.ROOT_DIR, 'exemplul_2.ui')
        loadUi(ui_path, self)
        self.search_btn.clicked.connect(self.search)
        self.save_as_file_btn.clicked.connect(self.save_as_file)
        self.add_book_btn.clicked.connect(self.on_add_click)
        self.apache_mq = ApacheMQ(self)

    def set_response(self, response):
        self.result.setText(response)

    def send_request(self, request):
        self.apache_mq.send_message(message=request)
        self.apache_mq.receive_message()

    def search(self):
        search_string = self.search_bar.text()
        request = "search-"
        print_type = "print:"
        find_type = "find:none"

        if self.json_rb.isChecked():
            print_type = 'print:json'
        elif self.html_rb.isChecked():
            print_type = 'print:html'
        elif self.xml_rb.isChecked():
            print_type = 'print:xml'
        else:
            print_type = 'print:raw'

        if search_string:
            if self.author_rb.isChecked():
                find_type = 'find:author={}'.format(search_string)
            elif self.title_rb.isChecked():
                find_type = 'find:title={}'.format(search_string)
            else:
                find_type = 'find:publisher={}'.format(search_string)
        request += print_type+"," + find_type
        self.send_request(request)

    def save_as_file(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_path = str(
            QFileDialog.getSaveFileName(self,
                                        'Salvare fisier',
                                        options=options))
        if file_path:
            file_path = file_path.split("'")[1]
            if not file_path.endswith('.json') and not file_path.endswith(
                    '.html') and not file_path.endswith('.txt'):
                if self.json_rb.isChecked():
                    file_path += '.json'
                elif self.html_rb.isChecked():
                    file_path += '.html'
                else:
                    file_path += '.txt'
            try:
                with open(file_path, 'w') as fp:
                    if file_path.endswith(".html"):
                        fp.write(self.result.toHtml())
                    else:
                        fp.write(self.result.toPlainText())
            except Exception as e:
                print(e)
                QMessageBox.warning(self, 'Exemplul 2',
                                    'Nu s-a putut salva fisierul')
									
    def on_add_click(self):
        self.new_frame=AddBookWidget(self)

    def add_book(self, req):
        self.new_frame.close()
        request="add-"+req
        self.rabbit_mq.send_message(message=request)
    
        


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = LibraryApp()
    window.show()
    sys.exit(app.exec_())
