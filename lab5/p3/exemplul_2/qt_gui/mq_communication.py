from retry import retry
from stompest.config import StompConfig
from stompest.protocol import StompSpec
from stompest.sync import Stomp


class ApacheMQ:
    def __init__(self, ui):
        self.ui = ui
        self.publisher=None
        self.consumer=None
        self.publisher = Stomp(StompConfig('tcp://localhost:61613'))
        self.startConsumer()


    def startConsumer(self):
        self.consumer = Stomp(StompConfig('tcp://localhost:61613'))
        self.consumer.connect()
        self.consumer.subscribe('libraryapp1.topic', {StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL})


    def receive_message(self):
        frame = self.consumer.receiveFrame()
        self.ui.set_response(frame.body.decode("utf-8"))
        self.consumer.ack(frame)

    def send_message(self, message):
        self.publisher.connect()
        self.publisher.send('libraryapp.topic', message.encode())
        self.publisher.disconnect()
