package com.sd.laborator.services

import com.sd.laborator.interfaces.LibraryDAO
import com.sd.laborator.interfaces.LibraryPrinter
import com.sd.laborator.model.Book
import com.sd.laborator.model.Content
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LibraryDAOService: LibraryDAO {
    @Autowired
    private lateinit var libraryPrinter: LibraryPrinter

    private var books: MutableSet<Book> = mutableSetOf(
        Book(Content("Roberto Ierusalimschy","Preface. When Waldemar, Luiz, and I started the development of Lua, back in 1993, we could hardly imagine that it would spread as it did. ...","Programming in LUA","Teora")),
        Book(Content("Jules Verne","Nemaipomeniti sunt francezii astia! - Vorbiti, domnule, va ascult! ....","Steaua Sudului","Corint")),
        Book(Content("Jules Verne","Cuvant Inainte. Imaginatia copiilor - zicea un mare poet romantic spaniol - este asemenea unui cal nazdravan, iar curiozitatea lor e pintenul ce-l fugareste prin lumea celor mai indraznete proiecte.","O calatorie spre centrul pamantului","Polirom")),
        Book(Content("Jules Verne","Partea intai. Naufragiatii vazduhului. Capitolul 1. Uraganul din 1865. ...","Insula Misterioasa","Teora")),
        Book(Content("Jules Verne","Capitolul I. S-a pus un premiu pe capul unui om. Se ofera premiu de 2000 de lire ...","Casa cu aburi","Albatros"))
    )
    override fun getBooks(format:String):String {

        return when(format) {
            "html" -> libraryPrinter.printHTML(this.books)
            "json" -> libraryPrinter.printJSON(this.books)
            "raw" -> libraryPrinter.printRaw(this.books)
            "xml"  -> libraryPrinter.printXML(this.books)
            else -> "Not implemented"
        }
    }

    override fun addBook(book: Book) {
        this.books.add(book)
    }

    override fun findAllByAuthor(author: String, format: String):String {
         var filtered_books = (this.books.filter { it.hasAuthor(author) }).toSet()
        return when(format)
        {
            "json"-> this.libraryPrinter.printJSON(filtered_books)
            "html" ->this.libraryPrinter.printHTML(filtered_books)
            "raw" ->this.libraryPrinter.printRaw(filtered_books)
            "xml"  ->this.libraryPrinter.printXML(filtered_books)
            else ->  "Format not implemented"
        }
    }

    override fun findAllByTitle(title: String, format: String): String {
        var filtered_books = (this.books.filter { it.hasTitle(title) }).toSet()
        return when (format) {
            "json" -> this.libraryPrinter.printJSON(filtered_books)
            "html" -> this.libraryPrinter.printHTML(filtered_books)
            "raw" -> this.libraryPrinter.printRaw(filtered_books)
            "xml" ->this.libraryPrinter.printXML(filtered_books)
            else -> "Format not implemented"
        }
    }

    override fun findAllByPublisher(publisher: String, format: String): String {
        var filtered_books =(this.books.filter { it.publishedBy(publisher) }).toSet()
        return when (format) {
            "json" ->this.libraryPrinter.printJSON(filtered_books)
            "html" -> this.libraryPrinter.printHTML(filtered_books)
            "raw" -> this.libraryPrinter.printRaw(filtered_books)
            "xml" -> this.libraryPrinter.printXML(filtered_books)
            else -> " Format not implemented"
        }

    }
}