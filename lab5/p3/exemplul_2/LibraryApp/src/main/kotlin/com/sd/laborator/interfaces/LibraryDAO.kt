package com.sd.laborator.interfaces

import com.sd.laborator.model.Book

interface LibraryDAO {
    fun getBooks(format:String): String
    fun addBook(book: Book)
    fun findAllByAuthor(author: String, format:String): String
    fun findAllByTitle(title: String, format:String):String
    fun findAllByPublisher(publisher: String, format:String): String
}