package com.sd.laborator.components

import com.sd.laborator.interfaces.LibraryDAO
import com.sd.laborator.interfaces.LibraryPrinter
import com.sd.laborator.model.Book
import com.sd.laborator.model.Content
import org.apache.activemq.Message
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jms.annotation.JmsListener
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Component
import java.lang.Exception
import javax.jms.JMSException
import javax.jms.TextMessage

@Component
class LibraryAppComponent {
    @Autowired
    private lateinit var libraryDAO: LibraryDAO

    @JmsListener(destination="libraryapp.topic")
    @SendTo("libraryapp1.topic")
    @Throws(JMSException::class)
    fun receiveMessage(jsonMessage : Message):String? {
        // the result needs processing
        var messageData: String? = null
        var response: String? = null

        try {
            var messageData=""
            if(jsonMessage is TextMessage) {
                var textMessage = jsonMessage as TextMessage
                messageData= textMessage.text

            }

            val (functie, body) = messageData!!.split("-")

            when(functie) {

                "add" -> {
                    val (nume, autor, editura, continut) = body.split(",")
                    val (numef, nume_val) = nume.split(":")
                    val (autorf, autor_val) = autor.split(":")
                    val (editf, edit_val) = editura.split(":")
                    val (contf, cont_val) = continut.split(":")

                    var content: Content = Content(autor_val,cont_val, nume_val,  edit_val)
                    var b: Book = Book(content)
                    addBook(b)
                }
                "search" -> {
                    val (print , find) = body.split(",")
                    val (printF , print_type) = print.split(":")
                    val (findF , find_p)= find.split(":")
                    val result  : String   ? = when (find_p) {
                        "none" -> return customPrint(print_type)
                        else -> return customFind(find_p, print_type)
                    }
                }
            }
            }
            catch (e: Exception) {
            println(e)
        }
        return response
    }

    fun customPrint(format: String): String {

        return libraryDAO.getBooks(format)
    }

    fun customFind(searchParameter: String, format:String ): String {
        val (field, value) = searchParameter.split("=")
        return when(field) {
            "author" ->{
                libraryDAO.findAllByAuthor(value, format)
            }
            "title" -> {
                this.libraryDAO.findAllByTitle(value, format)
            }
            "publisher" -> {
                this.libraryDAO.findAllByPublisher(value,format)
            }
            else -> "Not a valid field"
        }
    }

    fun addBook(book: Book): Boolean {
        return try {
            this.libraryDAO.addBook(book)
            true
        } catch (e: Exception) {
            false
        }
    }

}