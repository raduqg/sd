import org.apache.activemq.ActiveMQConnectionFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.config.DefaultJmsListenerContainerFactory
import org.springframework.jms.core.JmsTemplate


@Configuration
open class JmsConfig {
    var BROKER_URL = "tcp://localhost:61616"
    var BROKER_USERNAME = "admin"
    var BROKER_PASSWORD = "admin"

    @Bean
    open fun connectionFactory(): ActiveMQConnectionFactory {
        val connectionFactory = ActiveMQConnectionFactory()
        connectionFactory.brokerURL = BROKER_URL
        connectionFactory.password = BROKER_USERNAME
        connectionFactory.userName = BROKER_PASSWORD
        return connectionFactory
    }

    @Bean
    open fun jmsTemplate(): JmsTemplate {
        val template = JmsTemplate()
        template.connectionFactory = connectionFactory()
        template.setPubSubDomain(true)
        return template
    }

    @Bean
    open fun jmsListenerContainerFactory(): DefaultJmsListenerContainerFactory {
        val factory = DefaultJmsListenerContainerFactory()
        factory.setConnectionFactory(connectionFactory())
        factory.setConcurrency("1-1")
        factory.setPubSubDomain(true)
        return factory
    }
}