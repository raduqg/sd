package com.sd.laborator

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.jms.annotation.EnableJms


@SpringBootApplication
@EnableJms
open class LibraryApp

fun main(args: Array<String>) {
    runApplication<LibraryApp>(*args)
}
