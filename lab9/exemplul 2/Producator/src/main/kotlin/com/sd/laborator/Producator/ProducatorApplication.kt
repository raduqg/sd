package com.sd.laborator.Producator

import com.sd.laborator.Producator.components.RabbitMqComponent
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class ProducatorApplication
{

@Autowired
private lateinit var rabbitMqComponent: RabbitMqComponent

private lateinit var amqpTemplate: AmqpTemplate

@Autowired
fun initTemplate() {
    this.amqpTemplate = rabbitMqComponent.rabbitTemplate()
}


@RabbitListener(queues = ["\${depozit.rabbitmq.queue}"])
fun recieveMessage(msg: String) {

    println("I received $msg")

}


}



fun main(args: Array<String>) {
    runApplication<ProducatorApplication>(*args)
}
