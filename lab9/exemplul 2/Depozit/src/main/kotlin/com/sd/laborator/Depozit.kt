package com.sd.laborator

import com.sd.laborator.services.DepozitDAOService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Processor
import org.springframework.integration.annotation.Transformer
import org.springframework.messaging.support.MessageBuilder
import kotlin.random.Random

@EnableBinding(Processor::class)
@SpringBootApplication
open class DepozitMicroservice {

    @Autowired
    private lateinit var depozitDAOService: DepozitDAOService

    private fun acceptareComanda(identificator: Long, idProdus: Long, cantitate: Int): String {
        println("Comanda cu identificatorul $identificator a fost acceptata!")
        depozitDAOService.shouldCommandProduct(idProdus)
        return pregatireColet(idProdus, cantitate)
    }

    private fun respingereComanda(identificator: Long): String {
        println("Comanda cu identificatorul $identificator a fost respinsa! Stoc insuficient.")

        return "RESPINSA"
    }

    private fun verificareStoc(idProdus:Long, cantitate: Int): Boolean {

        val stoc = depozitDAOService.getStoc(idProdus)
        if(stoc>=cantitate)
            return true
        depozitDAOService.sendRestockMessage(idProdus)
        return false
    }

    private fun pregatireColet(produs: Long, cantitate: Int): String {
        ///TODO - retragere produs de pe stoc in cantitatea specificata
        depozitDAOService.retrageProdus(produs, cantitate)
        println("Produsul $produs in cantitate de $cantitate buc. este pregatit de livrare.")
        return "$produs|$cantitate"
    }

    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
    ///TODO - parametrul ar trebui sa fie doar numarul de inregistrare al comenzii si atat
    fun procesareComanda(idComanda: Long?): String {
        println("Procesez comanda cu identificatorul $idComanda...")
        val comanda = depozitDAOService.getDetaliiComanda(idComanda!!)

        //TODO - procesare comanda in depozit
        val rezultatProcesareComanda: String = if (verificareStoc(comanda.productId, comanda.cantitate)) {
            acceptareComanda(idComanda, comanda.productId, comanda.cantitate)
        } else {
            respingereComanda(idComanda)

        }

        ///TODO - in loc sa se trimita mesajul cu toate datele in continuare, trebuie trimis doar ID-ul comenzii
        return "$idComanda"
    }
}

fun main(args: Array<String>) {
    runApplication<DepozitMicroservice>(*args)
}