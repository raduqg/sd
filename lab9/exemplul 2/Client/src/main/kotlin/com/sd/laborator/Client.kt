package com.sd.laborator

import com.sd.laborator.model.Client
import com.sd.laborator.services.ClientDAOService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Source
import org.springframework.context.annotation.Bean
import org.springframework.integration.annotation.InboundChannelAdapter
import org.springframework.integration.annotation.Poller
import org.springframework.messaging.Message
import org.springframework.messaging.support.MessageBuilder
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.net.ServerSocket

@EnableBinding(Source::class)
@SpringBootApplication
open class ClientMicroservice {

    @Autowired
    private lateinit var clientDAOService: ClientDAOService
    
    private lateinit var clientSocket: ServerSocket


    companion object Constants {
        const val CLIENT_PORT = 1600
    }

    @Bean
    @InboundChannelAdapter(value = Source.OUTPUT, poller = [Poller(fixedDelay = "10000", maxMessagesPerPoll = "1")])
    open fun comandaProdus(): () -> Message<String> {
        return {
            var mesaj =""
            clientSocket = ServerSocket(CLIENT_PORT)

            val clientConnection = clientSocket.accept()
            print("S-a conecatat un client")

            val clientBufferReader = BufferedReader(InputStreamReader(clientConnection.inputStream))
            val receivedQuestion = clientBufferReader.readLine()

            println(receivedQuestion)

            val client_data =receivedQuestion.split(",")
            println(client_data)

            val produs = client_data[5]

            val cantitate = client_data[6].toInt()

            if(clientDAOService.productExists(produs)) {

                val client = Client(client_data[0], client_data[1], client_data[2], client_data[3], client_data[4])

                val id = clientDAOService.inregistreaza(client)


                mesaj = "$id|$produs|$cantitate"
            }
            clientSocket.close()

            MessageBuilder.withPayload(mesaj).build()
        }
    }
}


fun main(args: Array<String>) {
    runApplication<ClientMicroservice>(*args)
}