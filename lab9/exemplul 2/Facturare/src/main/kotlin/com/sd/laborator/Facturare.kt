package com.sd.laborator

import com.sd.laborator.services.FacturaDAOService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Processor
import org.springframework.integration.annotation.Transformer
import kotlin.random.Random

@EnableBinding(Processor::class)
@SpringBootApplication
open class FacturareMicroservice {

    @Autowired
    private lateinit var facturaDAOService: FacturaDAOService

    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)

    fun emitereFactura(comanda: String?): String {
        val idComanda = comanda!!.toLong()
        println("Emit factura pentru comanda $idComanda...")
        val nrFactura =facturaDAOService.inregistrareFactura(idComanda)
        println("S-a emis factura cu nr $nrFactura.")

        return "$comanda"
    }
}

fun main(args: Array<String>) {
    runApplication<FacturareMicroservice>(*args)
}