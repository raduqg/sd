package com.sd.laborator

import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.api.java.function.Function
import org.apache.spark.api.java.function.Function2
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.storage.StorageLevel
import java.io.File

fun readFileDirectlyAsText(fileName: String): String
        = File(fileName).readText(Charsets.UTF_8)

fun main(args: Array<String>) {
    // configurarea Spark
    val sparkConf = SparkConf().setMaster("local[*, 6]").setAppName("Spark Example")
    // initializarea contextului Spark
    val sparkContext = JavaSparkContext(sparkConf)

    // seturi de date externe
    // setul de date nu este inca incarcat in memorie (si nu se actioneaza inca asupra lui)
    val lines = sparkContext.textFile("src/main/resources/ebook.txt")
    println(lines)
    // pentru utilizarea unui RDD de mai multe ori, trebuie apelata metoda persist:
    lines.persist(StorageLevel.MEMORY_ONLY())

    // functia de mapare reprezinta o transformare a setului de date initial (nu este calculat imediat)
    // abia cand se ajunge la functia de reducere (care este o actiune) Spark imparte operatiile in task-uri
    // pentru a fi rulate pe masini separate (fiecare masina executand o parte din map si reduce)
    // exemplu cu functii lambda:
//    val totalLength0 = lines.map { s->s.length }.reduce { a: Int, b: Int -> a + b }
//    val re = Regex("[^A-Za-z ]")
//    val line: List<Char> = lines.toList()

    // trimiterea unor functii catre Spark
    val totalLength1= lines.map(object : Function<String?, Int?> {
            override fun call(p0: String?): Int? {
                return p0?.length ?: 0
            }
        }).reduce(object : Function2<Int?, Int?, Int?> {
            override fun call(p0: Int?, p1: Int?): Int? {
                return (p0?.toInt() ?: 0) + (p1?.toInt() ?: 0)
            }
        })

    // sau daca scrierea functiilor inline sau a celor lambda este greoaie, se pot utiliza clase
    val totalLength2 = lines.map(GetLength()).reduce(Sum())
//    println(totalLength0)
    println(totalLength1)
    println(totalLength2)

    // variabila partajata de tip broadcast
    // trimiterea unui set de date ca input catre fiecare nod intr-o maniera eficienta:
    val broadcastVar: Broadcast<List<Int>> = sparkContext.broadcast(listOf(1, 2, 3))
    val totalLength3 = lines.map { s->s.length + broadcastVar.value()[0] }.reduce { a: Int, b: Int -> a + b }
    println(totalLength3)

    //variabila partajata de tip acumulator
    val accumulator = sparkContext.sc().longAccumulator()
    sparkContext.parallelize(listOf(1, 2, 3, 4)).foreach { x -> accumulator.add(x.toLong()) }
    println(accumulator)

    // oprirea contextului Spark
    sparkContext.stop()
}