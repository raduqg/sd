#!/usr/bin/env python
"""reducer.py"""

import sys

url_hrefs = {}

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    url, _ = line.split()  # line.split('\t', 1)
    if url not in url_hrefs.keys():
        url_hrefs[url] = 1
    else:
        url_hrefs[url] = url_hrefs[url] + 1


for url, count in url_hrefs.items():
    print("%s\t%s\n" % (url, str(count)))