#!/usr/bin/env python
"""mapper.py"""

import sys
import urllib.request
from bs4 import BeautifulSoup
import sqlite3
import os

conn = sqlite3.connect(os.path.expanduser('~')+'/.mozilla/firefox/l3ourji7.default/places.sqlite')

c = conn.cursor()
c.execute('SELECT * FROM moz_places')
list_history = c.fetchall()
history = []
for i in list_history:
	it = str(i[1].split('://')[0]) + "://" + str(i[1].split('://')[1].split('/')[0])
	history.append(it)

hist_dict={}

for url in history:
    print('%s\t%s' % (url, "1"))
