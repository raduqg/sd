#!/usr/bin/env python
"""mapper.py"""

import sys
import urllib.request
from bs4 import BeautifulSoup

# path = "input/urls.txt"
# f = open(path, "r")

# input comes from STDIN (standard input)
for line in sys.stdin:
#for line in f:
    # remove leading and trailing whitespace
    line = line.strip()

    fp = urllib.request.urlopen(line)
    
    soup = BeautifulSoup(fp, "html.parser")
    for link in soup.findAll('a'):
        print('%s\t%s' % (line, link.get('href')))
