#!/usr/bin/env python
"""reducer.py"""

import sys

current_word = None
current_count = 0
word = None
letter_word = {}

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    first_letter, word = line.split()  # line.split('\t', 1)
    if first_letter not in letter_word.keys():
        letter_word[first_letter] = [word]
    else:
    	if word not in letter_word[first_letter]:
	        letter_word[first_letter].append(word)


for letter, words in letter_word.items():
    print("%s\t%s\n" % (letter, str(words)))
